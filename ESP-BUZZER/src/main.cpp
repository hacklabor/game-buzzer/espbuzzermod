
/*
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp-now-esp8266-nodemcu-arduino-ide/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*/

#include <ESP8266WiFi.h>
#include <espnow.h>

// REPLACE WITH RECEIVER MAC Address
uint8_t broadcastAddress[] = {0xE0,0x98,0x06,0x19,0x72,0xE8};
static const uint8_t  button = D8;
static const uint8_t  LED_SOUND = D7;
unsigned long SpamTimeout = 2500; //in ms
unsigned int id = 2; //  1=gelb 2=grün 3=rot 4=blau

// Structure example to send data
// Must match the receiver structure
typedef struct struct_message {
   unsigned int id;
   uint8_t macAddress[6];
  
  } struct_message;

// Create a struct_message called myData
struct_message myData;
struct_message myRecData;

unsigned long lastTime = 0;  
unsigned long lastButtonTime=0;
unsigned long timerDelay = 2000;  // send readings timer

// Callback when data is sent
void OnDataSent(uint8_t *mac_addr, uint8_t sendStatus) {
  Serial.print("Last Packet Send Status: ");
  if (sendStatus == 0){
    Serial.println("Delivery success");
  }
  else{
    Serial.println("Delivery fail");
  }
}
// Callback function that will be executed when data is received
void OnDataRecv(uint8_t * mac, uint8_t *incomingData, uint8_t len) {

  memcpy(&myRecData, incomingData, sizeof(myRecData));
  Serial.print("Bytes received: ");
  Serial.println(len);
  Serial.print("Int: ");
  Serial.println(myRecData.id);
    
 

}
 
void setup() {
  // Init Serial Monitor
  Serial.begin(115200);
  delay(500);
    Serial.print("ESP8266 Board MAC Address:  ");
  
  Serial.println(WiFi.macAddress());
 
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
  myData.id = id;
  WiFi.macAddress(myData.macAddress);
  for (int i =0 ;i<6;i++){
    Serial.println(myData.macAddress[i]);
  }
  // Init ESP-NOW
  if (esp_now_init() != 0) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_set_self_role(ESP_NOW_ROLE_COMBO);
  esp_now_register_send_cb(OnDataSent);
  
  // Register peer
  esp_now_add_peer(broadcastAddress, ESP_NOW_ROLE_COMBO, 1, NULL, 0);
  esp_now_register_recv_cb(OnDataRecv);

  pinMode(button,INPUT);
   pinMode(LED_SOUND,OUTPUT);
   digitalWrite(LED_SOUND,LOW);
}
 
void loop() {
  if (digitalRead(button)==HIGH && millis()-lastButtonTime>SpamTimeout) {
    // Set values to send


    

    if (millis()-lastTime>5){
    // Send message via ESP-NOW
   
      esp_now_send(broadcastAddress, (uint8_t *) &myData, sizeof(myData));
     lastButtonTime=millis();
    
 
    
    }
 
  }
  else{
       lastTime = millis();
  }

 if (myRecData.id==id){
  myRecData.id=0;
      Serial.println("LED_HIGH");
    digitalWrite(LED_SOUND,HIGH);
    delay(500);
     digitalWrite(LED_SOUND,LOW);
     Serial.println("LED_LOW");
  }
  
 
  

}